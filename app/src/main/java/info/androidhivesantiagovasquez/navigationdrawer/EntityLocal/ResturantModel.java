package info.androidhivesantiagovasquez.navigationdrawer.EntityLocal;


import android.text.Editable;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;

@Table(name="Restaurant")
public class ResturantModel extends Model {
    public ResturantModel(){};

    @Column(name="name")
    public String name;

    @Column(name="description")
    public String description;

    @Column(name="address")
    public String address;

    @Column(name="isOpen")
    public String isOpen;

    @Column(name="phone")
    public String phone;

    @Column(name="city")
    public String city;

    @Column(name="latitude")
    public String latitude;

    @Column(name="longitude")
    public double longitude;

    @Column(name="image")
    public String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return String.valueOf(latitude);
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return String.valueOf(longitude);
    }

    public void setLongitude(String longitude) {
        this.longitude = Double.parseDouble(longitude);
    }

    public String getImage() {
        return image;
    }

    public int getTotal(){
        return  new Select().from(ResturantModel.class).execute().size();
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void saveRestaurant(String name, String description, String address, boolean isOpen, int phone, String city, String latitude, double longitude, String image){
                this.name = name;
                this.description = description;
                this.address = address;
                this.isOpen = String.valueOf(isOpen);
                this.phone = String.valueOf(phone);
                this.city = city;
                this.latitude = latitude;
                this.longitude = longitude;
                this.image = image;
                this.save();
    }

    public void editRestaurant(String adress, String city, String description, String name, String phone,String id){
        new Update(ResturantModel.class)
                .set(   "address = ?," +
                                "city = ? ," +
                                "description = ? ," +
                                "name = ? ," +
                                "phone = ? ",
                        adress,
                        city,
                        description,
                        name,
                        phone).where("id = ?",id)
                .execute();
    }

    public static List<ResturantModel> selectAll(){
        return new Select().from(ResturantModel.class).orderBy("name ASC").execute();
    }

    public static String removeRestaurant(String id){
        new Delete().from(ResturantModel.class).where("Id = ?", id).execute();
        return id;
    }

}

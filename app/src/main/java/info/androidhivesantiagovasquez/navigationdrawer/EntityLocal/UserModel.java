package info.androidhivesantiagovasquez.navigationdrawer.EntityLocal;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.List;

import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.User;

/**
 * Created by santiagovasquez on 2/05/2017.
 */
@Table(name="User")
public class UserModel extends Model {


        public UserModel(){};

        @Column(name="email")
        public String email;

        @Column(name="password")
        public String password;

        @Column(name="username")
        public String username;

        @Column(name="token")
        public String token;

        @Column(name="role")
        public String role;

        @Column(name="state")
        public String state;

        @Column(name="login")
        public String login;

        public UserModel getLogin() {
                return  new Select().from(UserModel.class).executeSingle();
        }

        public void setLogin(String login) {
                this.login = login;
        }

        public String getEmail() {
                UserModel userModel=new Select().from(UserModel.class).executeSingle();
                return userModel.email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getPassword() {
                UserModel userModel=new Select().from(UserModel.class).executeSingle();
                return userModel.password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public String getUsername() {
                UserModel userModel=new Select().from(UserModel.class).executeSingle();
                return userModel.username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getToken() {
                UserModel userMdl = new Select().from(UserModel.class).executeSingle();
                return userMdl.token;
        }

        public void setToken(String token) {
                this.token = token;
        }

        public String getRole() {
                return role;
        }

        public void setRole(String role) {
                this.role = role;
        }

        public String getState() {
                UserModel userMdl = new Select().from(UserModel.class).executeSingle();
                return userMdl.state;
        }

        public void setState(String state) {
                this.state = state;
        }


        public static List<UserModel> selectUser(){
                return new Select().from(UserModel.class).execute();
        }

        public int getTotal(){
                return  new Select().from(UserModel.class).execute().size();
        }

        public void updateUser(String email){
                this.email=email;
                new Update(UserModel.class)
                        .set("role=null, " + "state=null, " + "login=0").where("email = ? ",email)
                        .execute();
        }

        public void updateLoginUser(String email,String login){
                this.login=login;
                this.email=email;
                new Update(UserModel.class)
                        .set("login=1").where("email = ? ",email)
                        .execute();
        }

        public void saveUser(String email, String password, String role, String state, String token,String username){
                        ActiveAndroid.beginTransaction();
                        try {
                                for (int i = 0; i < 100; i++){
                                        this.email=email;
                                        this.password=password;
                                        this.role=role;
                                        this.state=state;
                                        this.token=token;
                                        this.username=username;
                                        this.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                        }finally {
                                ActiveAndroid.endTransaction();
                        }
        }
}

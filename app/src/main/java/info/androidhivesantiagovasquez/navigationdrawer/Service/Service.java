package info.androidhivesantiagovasquez.navigationdrawer.Service;

import android.database.Observable;

import java.util.List;

import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.Restaurant;
import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {

    //petición del login
    @GET("login?")
    Call<User> login(@Query("user") String email,@Query("password") String password);

    //petición para obtener restaurantes
    @GET("restaurants")
    Call<Restaurant> getUser(@Header("Auth-token") String token);
}

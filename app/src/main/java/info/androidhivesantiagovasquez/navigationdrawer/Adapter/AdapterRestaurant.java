package info.androidhivesantiagovasquez.navigationdrawer.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.Restaurant;
import info.androidhivesantiagovasquez.navigationdrawer.R;
import info.androidhivesantiagovasquez.navigationdrawer.activity.RestaurantDetailsActivity;
import info.androidhivesantiagovasquez.navigationdrawer.fragment.HomeFragment;

/**
 * Created by santiagovasquez on 3/05/2017.
 */
public class AdapterRestaurant extends RecyclerView.Adapter<AdapterRestaurant.ViewHolderRestaurant> {

    HomeFragment fragment;
    List<ResturantModel> restaurants;
    public AdapterRestaurant(List<ResturantModel> data) {
        this.restaurants=data;
    }
    private Uri mImageUri;


    @Override
    public ViewHolderRestaurant onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant, parent, false);
        final ViewHolderRestaurant holder = new ViewHolderRestaurant(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolderRestaurant holder, final int position) {
        holder.txt_name.setText(restaurants.get(position).getName());
        holder.txt_country.setText(restaurants.get(position).getAddress());
        holder.id = Integer.parseInt(String.valueOf(restaurants.get(position).getId()));
        holder.name =String.valueOf(restaurants.get(position).getName());
        holder.imagen = restaurants.get(position).getImage().toString();
        holder.description = restaurants.get(position).getDescription().toString();
        holder.phone =  restaurants.get(position).getPhone().toString();
        holder.city =  restaurants.get(position).getCity().toString();
        Picasso.with(holder.itemView.getContext()).load(holder.imagen).into(holder.imagenView);
        if (restaurants.get(position).getIsOpen().equals("true") ){
            holder.open.setImageResource(R.drawable.ic_check_circle_teal_400_24dp);
            holder.open.setVisibility(View.VISIBLE);
            holder.txt_state.setText("Abierto");
        }else{
            holder.open.setImageResource(R.drawable.ic_check_circle_red_500_24dp);
            holder.open.setVisibility(View.VISIBLE);
            holder.txt_state.setText("Cerrado");
        }

        //Picasso.with(fragment.getContext()).load(restaurants.get(position).getImage()).into(holder.imagen);
        //holder.imagen.setImageURI(Uri.parse(restaurants.get(position).getImage()));
    }
    static class ViewHolderRestaurant extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cart;
        TextView txt_name,txt_country,txt_state;
        ImageView open;
        ImageView imagenView;
        String imagen,name,description,phone,city;
        int id;
        public ViewHolderRestaurant(View itemView) {
            super(itemView);
            txt_country = (TextView) itemView.findViewById(R.id.txtCountry);
            txt_name = (TextView)itemView.findViewById(R.id.txtName);
            txt_state = (TextView)itemView.findViewById(R.id.txtState);
            imagenView = (ImageView) itemView.findViewById(R.id.imagen);
            open = (ImageView) itemView.findViewById(R.id.open);
            cart = (CardView) itemView.findViewById(R.id.cart);
            cart.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String idRestaurant = String.valueOf(id);
            Intent intent = new Intent(v.getContext(), RestaurantDetailsActivity.class);
            intent.removeExtra("id");
            intent.removeExtra("name");
            intent.removeExtra("img");
            intent.removeExtra("adress");
            intent.removeExtra("description");
            intent.removeExtra("phone");
            intent.removeExtra("city");
            intent.putExtra("id", idRestaurant);
            intent.putExtra("name", name);
            intent.putExtra("img", imagen);
            intent.putExtra("adress", txt_country.getText());
            intent.putExtra("description", description);
            intent.putExtra("phone", phone);
            intent.putExtra("city", city);
            Toast.makeText(v.getContext(),txt_name.getText(),Toast.LENGTH_SHORT).show();
            v.getContext().startActivity(intent);
        }
    }


    @Override
    public int getItemCount() {
        return restaurants.size();
    }

}

package info.androidhivesantiagovasquez.navigationdrawer.EntityRest;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by santiagovasquez on 3/05/2017.
 */
public class Restaurant {


    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @SerializedName("statusCode")
    public int statuscode;
    @SerializedName("state")
    public String state;
    @SerializedName("data")
    public Data data;

    public static class Restaurants {
        @SerializedName("name")
        public String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getPhone() {
            return phone;
        }

        public void setPhone(int phone) {
            this.phone = phone;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @SerializedName("description")
        public String description;
        @SerializedName("address")
        public String address;

        public boolean isopen() {
            return isopen;
        }

        public void setIsopen(boolean isopen) {
            this.isopen = isopen;
        }

        @SerializedName("isOpen")
        public boolean isopen;
        @SerializedName("phone")
        public int phone;
        @SerializedName("city")
        public String city;
        @SerializedName("latitude")
        public String latitude;
        @SerializedName("longitude")
        public double longitude;
        @SerializedName("image")
        public String image;
    }

    public static class Data {
        public List<Restaurants> getRestaurants() {
            return restaurants;
        }

        public void setRestaurants(List<Restaurants> restaurants) {
            this.restaurants = restaurants;
        }

        @SerializedName("restaurants")
        public List<Restaurants> restaurants;
    }
}

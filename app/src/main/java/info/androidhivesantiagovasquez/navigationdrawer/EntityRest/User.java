package info.androidhivesantiagovasquez.navigationdrawer.EntityRest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by santiagovasquez on 2/05/2017.
 */
public class User {
    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName("statusCode")
    public int statuscode;
    @SerializedName("state")
    public String state;
    @SerializedName("data")
    public Data data;

    public static class Data {
        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        @SerializedName("username")
        public String username;
        @SerializedName("token")
        public String token;
        @SerializedName("role")
        public String role;
    }
}

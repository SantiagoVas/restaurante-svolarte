package info.androidhivesantiagovasquez.navigationdrawer.DTO;


import info.androidhivesantiagovasquez.navigationdrawer.Util.Util;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {

    Util util = new Util();

    private info.androidhivesantiagovasquez.navigationdrawer.Service.Service service;

    public Client(){
        service = new Retrofit
                .Builder()
                .baseUrl(util.getAPI_URL().toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(info.androidhivesantiagovasquez.navigationdrawer.Service.Service.class);
    }

    public info.androidhivesantiagovasquez.navigationdrawer.Service.Service getService() {
        return service;
    }

    public void setService(info.androidhivesantiagovasquez.navigationdrawer.Service.Service service) {
        this.service = service;
    }
}

package info.androidhivesantiagovasquez.navigationdrawer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.R;

public class EditRestaurantActivity extends AppCompatActivity {

    String mId,mName,mDireccion,mCiudad,mTelefono,mDescripcion;
    EditText mNombre,mAdress,mCity,mPhone,mDescription;
    Button mGuardar;
    ResturantModel resturantModel = new ResturantModel();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_restaurant);
        iniciarVariables();
        //se reciben los parametros id,name
        Bundle bundle = getIntent().getExtras();
        mName = bundle.getString("name", mName).toString();
        mId = bundle.getString("id", mId);
        mDireccion = bundle.getString("adress", mDireccion).toString();
        mCiudad = bundle.getString("city", mCiudad).toString();
        mTelefono = bundle.getString("phone", mTelefono).toString();
        mDescripcion = bundle.getString("description", mDescripcion).toString();
        
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(mName);
        actionBar.setDisplayHomeAsUpEnabled(true);
        mNombre.setText(mName);
        mAdress.setText(mDireccion);
        mCity.setText(mCiudad);
        mPhone.setText(mTelefono);
        mDescription.setText(mDescripcion);
    }

    private void iniciarVariables() {
        mNombre = (EditText) findViewById(R.id.name);
        mCity = (EditText) findViewById(R.id.city);
        mAdress = (EditText) findViewById(R.id.address);
        mPhone = (EditText) findViewById(R.id.phone);
        mDescription = (EditText) findViewById(R.id.description);
        mGuardar = (Button) findViewById(R.id.btnGuardar);
        mGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resturantModel.editRestaurant(mAdress.getText().toString(),mCity.getText().toString(),mDescription.getText().toString(),mNombre.getText().toString(),mPhone.getText().toString(),mId.toString());
                Toast.makeText(EditRestaurantActivity.this,"Restaurante guardado correctamente",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(EditRestaurantActivity.this, LoginActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package info.androidhivesantiagovasquez.navigationdrawer.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.R;

public class RestaurantDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    String mId,mDescriptionn,mPhone,mCity;
    String mName;
    String mImg;
    String mAdress;
    TextView mNameRestaurant,mDescription,mTxtCity,mPhonee;
    ImageView mImageHeader;
    Context context;
    ResturantModel resturantModel = new ResturantModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);
        iniciarVariables();
        //se reciben los parametros id,name
        Bundle bundle = getIntent().getExtras();
        mName = bundle.getString("name", mName).toString();
        mId = bundle.getString("id", mId);
        mImg = bundle.getString("img", mImg).toString();
        mCity = bundle.getString("city", mCity).toString();
        mAdress= bundle.getString("adress", mAdress).toString();
        mDescriptionn = bundle.getString("description",mDescriptionn).toString();
        mPhone =bundle.getString("phone",mPhone).toString();
        mNameRestaurant.setText(mName);
        mTxtCity.setText(mAdress);
        mDescription.setText(mDescriptionn);
        mPhonee.setText(mPhone);
        Picasso.with(RestaurantDetailsActivity.this).load(mImg).into(mImageHeader);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(mName);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }



    private void iniciarVariables() {
        mImageHeader =(ImageView)findViewById(R.id.imageHeader);
        mNameRestaurant= (TextView) findViewById(R.id.nameRestaurant);
        mDescription = (TextView) findViewById(R.id.txtDescription);
        mTxtCity=(TextView)findViewById(R.id.txtCity);
        mPhonee = (TextView) findViewById(R.id.txtPhone);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.edit){
            String idRestaurant = String.valueOf(mId);
            Intent intent = new Intent(v.getContext(), EditRestaurantActivity.class);
            intent.putExtra("id", idRestaurant);
            intent.putExtra("name", mName);
            intent.putExtra("adress", mAdress);
            intent.putExtra("city", mCity);
            intent.putExtra("phone", mPhone);
            intent.putExtra("description", mDescriptionn);
            v.getContext().startActivity(intent);
        }else{
            new android.support.v7.app.AlertDialog.Builder(RestaurantDetailsActivity.this)
                    .setIcon(R.drawable.logo)
                    .setTitle("Eliminar restaurante")
                    .setMessage("¿Estas seguro que deseas eliminarlo?")
                    .setPositiveButton("Seguro", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            resturantModel.removeRestaurant(mId.toString());
                            Toast.makeText(RestaurantDetailsActivity.this,"Restaurante eliminado correctamente",Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RestaurantDetailsActivity.this, LoginActivity.class));
                        }

                    })
                    .setNegativeButton("Cancelar", null)
                    .show();
        }

    }
}

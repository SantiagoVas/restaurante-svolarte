package info.androidhivesantiagovasquez.navigationdrawer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;
import com.mobsandgeeks.saripaar.annotation.TextRule;

import java.util.List;

import info.androidhivesantiagovasquez.navigationdrawer.DTO.Client;
import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.UserModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.Restaurant;
import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.User;
import info.androidhivesantiagovasquez.navigationdrawer.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,Validator.ValidationListener {
    Button mLogin;
    TextView mtextView;
    Client client = new Client();
    ResturantModel resturant= new ResturantModel();
    UserModel userModel=new UserModel();
    protected Validator validator;
    protected boolean validated;
    String mValue;

    @Required(order = 1,message = "El correo electrónico es invalido")
    @Email(order =2,message = "el correo electrónico es invalido")
    private EditText mEmail;

    @Required(order = 3,message = "La contraseña es requerido")
    @TextRule(order = 4,minLength = 6,message = "mínimo 6 caracteres")
    private EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int totalItems = userModel.getTotal();

        if (totalItems < 1){

            userModel.login = "0"; // 0:logout  1:login
            userModel.save();
            //iniciarVariables();

        }else if(totalItems == 1){
            userModel = userModel.getLogin();
            if(!userModel.login.equals("0") ){
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(LoginActivity.this,"Sessión iniciada",Toast.LENGTH_SHORT).show();
            }else{
                //iniciarVariables();
            }

        }


        setContentView(R.layout.activity_login);
        iniciarVariables();
    }

    private void searchRestaurants(String mValue) {
        client.getService().getUser(mValue).enqueue(new Callback<Restaurant>() {
            @Override
            public void onResponse(Call<Restaurant> call, Response<Restaurant> response) {
                Restaurant restaurant = response.body();
                if (restaurant.getStatuscode() == 200 & restaurant.getState().equals("success")){
                    for (int i=0; i<restaurant.data.restaurants.size(); i++) {
                        ResturantModel resturantModel = new ResturantModel();
                        Restaurant.Restaurants restaurante = restaurant.data.restaurants.get(i);
                        resturantModel.saveRestaurant(restaurante.getName(),restaurante.getDescription(),restaurante.getAddress(),restaurante.isopen(),restaurante.getPhone(),restaurante.getCity(),restaurante.getLatitude(),restaurante.getLongitude(),restaurante.getImage());
                    }
                }else{
                    Toast.makeText(LoginActivity.this,"error al cargar los restaurantes, vuelve a intentarlo",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Restaurant> call, Throwable t) {

            }
        });
    }

    private void iniciarVariables() {
        validator = new Validator(this);
        validator.setValidationListener(this);
        mEmail=(EditText)findViewById(R.id.input_email);
        mPassword=(EditText)findViewById(R.id.input_password);
        mtextView=(TextView)findViewById(R.id.register);
        mLogin = (Button)findViewById(R.id.btnlogin);
        mEmail.setText("prueba@prueba.com");
        mPassword.setText("abcd1234");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnlogin){
            validator.validate();
        }
    }

    public void addUser(final String email, final String password) {
        client.getService().login(email,password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                response.body();
                User user = response.body();
                if (user.getState().equals("success") && user.getStatuscode() == 200 ){
                    userModel.saveUser(email.toString(),password.toString(),user.data.getRole(),user.getState(),user.data.getToken(),user.data.getUsername());
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(),"Bien, sesión iniciada correctamente",Toast.LENGTH_SHORT).show();
                    userModel.updateLoginUser(email.toString(),"1");
                        int totalItems = resturant.getTotal();
                        if (totalItems < 1){
                            mValue = userModel.getToken().toString();
                            searchRestaurants(mValue);
                        }
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Error, usuario o contraseña incorrecta",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                List<UserModel> user =userModel.selectUser();
                if (user.size() > 0){
                    for (int i=0; i<user.size(); i++) {
                        UserModel userModel = new UserModel();
                        String correo = userModel.getEmail();
                        String pass = userModel.getPassword();
                        if (correo != null && pass != null){
                            if (email.toString().equals(correo.toString()) && password.toString().equals(pass.toString())){
                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(intent);
                                userModel.updateLoginUser(email.toString(),"1");
                                Toast.makeText(getApplicationContext(),"Bien, sesión iniciada correctamente",Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getApplicationContext(),"Error, usuario o contraseña incorrecta",Toast.LENGTH_SHORT).show();
                            }
                        }else{

                        }

                    }
                }

                Toast.makeText(getApplicationContext(),"sin conexión a la red",Toast.LENGTH_SHORT).show();
            }
        });
    }

    //cuando es success el formulario
    @Override
    public void onValidationSucceeded() {
        validated= true;
        addUser(mEmail.getText().toString(),mPassword.getText().toString());
    }

    //cuando es error el formulario
    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        final String failureMessage = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            EditText failed = (EditText) failedView;
            failed.requestFocus();
            failed.setError(failureMessage);
        }else{
            Toast.makeText(getApplicationContext(), failureMessage, Toast.LENGTH_SHORT).show();
        }
    }


}

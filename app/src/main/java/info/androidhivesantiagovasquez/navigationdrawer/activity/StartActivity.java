package info.androidhivesantiagovasquez.navigationdrawer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

import info.androidhivesantiagovasquez.navigationdrawer.R;

public class StartActivity extends AppCompatActivity {

    private static final long SPLASH_SCREEN_DELAY = 2000;
    private ImageView mImg;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_start);

            mImg = (ImageView) findViewById(R.id.logo);

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    Intent mainActivity = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(mainActivity);
                    finish();


                    runOnUiThread(new Runnable() {
                        public void run() {

                        }
                    });

                }

            };


            Timer timer = new Timer();
            timer.schedule(task, SPLASH_SCREEN_DELAY);
        }
    }

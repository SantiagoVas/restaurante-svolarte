package info.androidhivesantiagovasquez.navigationdrawer;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.UserModel;

public class App extends Application {
    @Override
    public void onCreate(){
        super.onCreate();

        Configuration configuration = new Configuration.Builder(this).addModelClass(UserModel.class).addModelClass(ResturantModel.class).create();

        ActiveAndroid.initialize(configuration);

    }
}

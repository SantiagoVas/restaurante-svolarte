package info.androidhivesantiagovasquez.navigationdrawer.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import info.androidhivesantiagovasquez.navigationdrawer.Adapter.AdapterRestaurant;
import info.androidhivesantiagovasquez.navigationdrawer.DTO.Client;
import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.ResturantModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityLocal.UserModel;
import info.androidhivesantiagovasquez.navigationdrawer.EntityRest.Restaurant;
import info.androidhivesantiagovasquez.navigationdrawer.R;
import info.androidhivesantiagovasquez.navigationdrawer.activity.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Client client = new Client();
    UserModel userModel = new UserModel();
    ResturantModel resturant= new ResturantModel();
    private RecyclerView mRecyclerView;


    private String mParam1;
    private String mParam2;
    private String mValue;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mRecyclerView= (RecyclerView) view.findViewById(R.id.lista);
        int totalItems = resturant.getTotal();
        mValue = userModel.getToken().toString();
        if (totalItems < 1){
            //searchRestaurants(mValue);
        }else{
            List<ResturantModel> data = resturant.selectAll();
            mRecyclerView.scrollToPosition(0);
            mRecyclerView.setHasFixedSize(true);
            if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
            }
            else{
                mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            }
            mRecyclerView.setAdapter(new AdapterRestaurant(data));
        }







        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
